<?php

class Login_Model extends CI_Model {

     
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function authenticate_user(){
		
		$user_email				 =	$this->db->escape_str($this->input->post('email'));
		$user_pass				  =	$this->db->escape_str($this->input->post('pass'));
		
		
			
		$this->db->where('tp_user_email',$user_email);
		$this->db->where('tp_user_password',md5($user_pass));
		$user = $this->db->get('tp_users')->row();
		 
		if ( count($user) == 1 ) {
			 
			 $sessiondata = array(
			 	   '_USER_ID'	  	   => $user->tp_user_id,		
                   '_USER_NAME'  		 => $user->tp_user_fname.' '.$user->tp_user_lname,
                   '_USER_EMAIL'     	=> $user->tp_user_email,
                   '_USER_LOGGED_IN' 	=> TRUE
               );

			 $this->session->set_userdata($sessiondata);

			 redirect('home');
			 
		} else {
			 
			 $this->template->write('error_template','
			 <p align="center">The email or password you entered is incorrect. Please try again (make sure capslock is off)</p>'
			 );
		}
		
			
		//$this->template->write('error_template','Oops! Something went wrong in the user registration');	
		 
		 
	}
    
	
}


