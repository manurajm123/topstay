<?php
class General_Model extends CI_Model {
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function get_data( $table, $fields = '*', $where = array(),$order_by = '' ){
		if((is_array($where) && count($where)>0) or (!is_array($where) && trim($where) != '')) $this->db->where($where);
		if($order_by) $this->db->order_by($order_by);
		$this->db->select($fields);
		$query = $this->db->get($table);
		return $query->result_array();
	}
	
	function insert($table, $fields = array()){
		if(count($fields) <= 0) return false;
		$this->db->insert( $table, $fields );
		return $this->db->insert_id();
	}
	
	function update($table, $fields = array(), $where = array()){
		if(count($fields) <= 0) return false;
		if(is_array($where) and count($where) > 0) $this->db->where($where);
		return $this->db->update( $table, $fields );
		
	}
	function delete($table, $where){
		$this->db->where($where);
		return $this->db->delete( $table );
	}
	
	function truncate_table($table)
	{
		$this->db->empty_table($table);
	}
	
	function prepare_select_box_data( $table, $fields, $where = array(), $insert_null = false,$order_by = ''){
		
		list($key, $val) 	= explode(',',$fields);
		$key 				= trim($key);
		$val 				= trim($val);
		$order_by			= $order_by ? $order_by : $val;
		$input_array 		= $this->get_data( $table, $fields, $where, $order_by );
		
		$select_box_array 	= array();
		$total_records 		= count($input_array);
		if($insert_null) {
			$select_box_array[] = $insert_null===true ? '' : $insert_null;
		}
		for($i = 0; $i < $total_records; $i++){
		 	$select_box_array[$input_array[$i][$key]] = $input_array[$i][$val];
		}
		return $select_box_array;
	}
}
?>