<?php

class Register_Model extends CI_Model {

     
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function saveuser(){
		
		$data['tp_user_email']				 =	$this->db->escape_str($this->input->post('email'));
		$data['tp_user_fname']				 =	$this->db->escape_str($this->input->post('fname'));
		$data['tp_user_lname']				 =	$this->db->escape_str($this->input->post('lname'));
		$data['tp_user_mobile']		 		=	$this->db->escape_str($this->input->post('mobilenumber'));
		$data['tp_user_landline']			  =	$this->db->escape_str($this->input->post('phonenum'));
		$data['tp_user_ip']					=	$this->input->ip_address();
		$data['tp_user_password']			  =	md5($this->db->escape_str($this->input->post('pass')));
		$data['tp_user_created_on']			=	date('Y-m-d H:i:s');
		
		$this->db->insert('tp_users',$data);
		
		if($this->db->affected_rows() == 1 ){
			
			redirect('register/register_success', 'refresh');
			
		} else {
			
			$this->template->write('error_template','Oops! Something went wrong in the user registration');	
		}
		 
	}
    
	
}


