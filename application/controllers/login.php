<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function __construct(){
		
		parent::__construct();
		$this->Contents  =	array();
		$this->load->model('Login_Model');
		$this->template->add_js('resources/js/user.js');
	}
	
	public function index()
	{
		
		if(array_key_exists('login-submit',$_POST)){
			
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean');
			$this->form_validation->set_rules('pass', 'Password', 'trim|required|xss_clean|min_length[5]');
			
			if ($this->form_validation->run() == false)			// if validation errors..
			{
				$this->template->write('error_template',validation_errors());
			}
			else
			{
				$this->Login_Model->authenticate_user();
					
			}
			
		}
		
		$this->template->write_view('content', 'public_user/user_login', $this->Contents);
		$this->template->render();
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */