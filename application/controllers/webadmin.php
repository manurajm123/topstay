<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Webadmin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct(){
		
		parent::__construct();
		$this->Contents  =	array();	
	}
	
	public function index()
	{
		
		$this->template->write_view('content', 'webadmin/webadmin_login', $this->Contents); 
		$this->template->render();
	}

	public function add_room()
	{
		
		$this->template->write_view('content', 'webadmin/add_room', $this->Contents); 
		$this->template->render();
	}
	 
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */