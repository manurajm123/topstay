<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		
		parent::__construct();
		$this->Contents  =	array();	

	}
	
	public function index()
	{
		
		// get the search item as autocomplete in search box.. 
		$this->template->add_js('resources/js/map.js');

		$this->template->write_view('slide_show', 'slide-show', $this->Contents);
		$this->template->write_view('content', 'home', $this->Contents);
		$this->template->render();
	}
	
	public function all()
	{
		 
		$this->template->render();
	}
	
	public function popular()
	{
		 
		$this->template->render();
	}
	
	public function friends()
	{
		 
		$this->template->render();
	}
	
	public function neighborhood()
	{
		 
		$this->template->render();
	}
	
	public function about()
	{
		$this->template->write_view('content', 'public_user/about', $this->Contents); 
		$this->template->render();
	}
	
	public function help()
	{
		 
		$this->template->render();
	}
	
	 
	 
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */