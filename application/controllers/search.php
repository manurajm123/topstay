<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct(){
		
		parent::__construct();
		$this->Contents  =	array();	
	}
	
	public function index( )
	{
		
		$query		=	$this->db->escape_str($this->input->get('query'));

		$lc_lat		=	$this->db->escape_str($this->input->get('lc_lat'));

		$lc_lng		=	$this->db->escape_str($this->input->get('lc_lng'));
  
		if( empty($query) || empty($lc_lat) || empty($lc_lng) ) redirect('welcome');

 		$this->Contents['query']		=	$query;
 		$this->Contents['lc_lat']		=	$lc_lat;
 		$this->Contents['lc_lng']		=	$lc_lng;
 
		$this->template->write_view('content', 'public_user/place-filter', $this->Contents); 
		$this->template->render();
	}
	 
   	
	   	
 	 
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */