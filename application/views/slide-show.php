   

 <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
   
 <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>

 <div class="slideshow  has-offset-slide">

  <div class="slides ">

   
   <form method="post" id="searchfrm" name="searchfrm"> 

    <div class="slideshow-slide">

      <div class="slide-navigation ">
        <span class="previous">&#57350;</span>
        <span class="next">&#57352;</span>
      </div>

      <img alt="" src="<?php echo _IMAGE_PATH;?>slide-image-6.jpg?777">
 
        <div class="action offset uk-text-left" >
          
           <h1 class="text-special">Find a place to stay.</h1>
           <p class="text-sub">Rent from people in over 34,000 cities and 192 countries.</p>
           <p>
                <input type="text" class="searchbox" name="query" id="query0"  placeholder="Where do you want to go?" autocomplete="off"/>
                <input type="submit" value="Search" class="searchbutton">
           </p>

        </div>
       
    </div><!-- close slide -->
    

    
    <div class="slideshow-slide">

      <div class="slide-navigation ">
        <span class="previous">&#57350;</span>
        <span class="next">&#57352;</span>
      </div>

      <img alt="" src="<?php echo _IMAGE_PATH;?>slide-image-7.jpg?777">
 
        <div class="action offset uk-text-left" >
          
           <h1 class="text-special">Find a place to stay.</h1>
           <p class="text-sub">Rent from people in over 34,000 cities and 192 countries.</p>
           <p>
                <input type="text" class="searchbox" name="query" id="query1"  placeholder="Where do you want to go?" autocomplete="off"/>
                <input type="submit" value="Search" class="searchbutton">
           </p>

        </div>
       
      
    </div><!-- close slide -->
    

	<div class="slideshow-slide">

      <div class="slide-navigation ">
        <span class="previous">&#57350;</span>
        <span class="next">&#57352;</span>
      </div>

      <img alt="" src="<?php echo _IMAGE_PATH;?>slide-image-8.jpg?777">
 
        <div class="action offset uk-text-left" >
          
           <h1 class="text-special">Find a place to stay.</h1>
           <p class="text-sub">Rent from people in over 34,000 cities and 192 countries.</p>
           <p>
                <input type="text" class="searchbox" name="query" id="query2"  placeholder="Where do you want to go?" autocomplete="off"/>
                <input type="submit" value="Search" class="searchbutton">
           </p>
                
        </div>
        
     
      
      
    </div><!-- close slide -->
     
   </form>    

    

  </div><!-- close SLIDES -->

</div>