

  <div class="welcome-message">
    <h1>Welcome to TopStay</h1>
    <div>Not sure where to stay? We've created neighborhood guides for cities all around the world.</div>

    
  </div>
 
 
  <div class="featured-collections collection-list grid-wrap grid-of-3">

    
      <h4 class="section-title">Neighborhood Guides</h4>
      <p>Not sure where to stay? We've created neighborhood guides for cities all around the world.</p>

    

      
      <!-- this class gets used in the featured products section below
           but must be set here -->
      

      <div class="items-wrap">
 

        <article class="collection-list-item centered">
          <a href="/collections/workspaces">
            <img alt="" src="<?php echo _IMAGE_PATH;?>wroks-aces-1_grande.jpg?v=1378240202">
            <h2>New York</h2>
          </a>
          
            <div class="rte">Maecenas faucibus mollis interdum.</div>
          
        </article>
      
 

        <article class="collection-list-item centered">
          <a href="/collections/storage">
            <img alt="" src="<?php echo _IMAGE_PATH;?>storage_grande.jpg?v=1378239497">
            <h2>London</h2>
          </a>
          
            <div class="rte">Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</div>
          
        </article>
      

      

        

        <article class="collection-list-item centered">
          <a href="/collections/seating">
            <img alt="" src="<?php echo _IMAGE_PATH;?>seating_grande.jpg?v=1378239096">
            <h2>Paris</h2>
          </a>
          
            <div class="rte">Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</div>
          
        </article>
      

      

      </div>

    
  </div>





  <div class="recent-posts grid-wrap grid-of-3 clearfix">

  
    <h4 class="section-title">From the blog</h4>

    

    <div class="items-wrap">

      
      <article>
        <h1><a class="title" href="/blogs/news/8944941-danish-modern-for-kids">Danish Modern for Kids</a></h1>
        <div class="rte content">
          
            <p><img src="<?php echo _IMAGE_PATH;?>geo-thumb.jpg?513" alt="" /></p>
<p><span>Nullam quis risus eget urna mollis ornare vel eu leo…</span></p>
<p></p>
          
        </div>
        <a class="date meta" href="/blogs/news/8944941-danish-modern-for-kids">Sep 04, 2013</a>
      </article>
      
      <article>
        <h1><a class="title" href="/blogs/news/8928423-dieter-rams">Dieter Rams</a></h1>
        <div class="rte content">
          
            <p><img src="<?php echo _IMAGE_PATH;?>rams-thumb.jpg?410" alt="" /></p>
<p><span>Donec ullamcorper nulla non metus auctor fringilla.</span></p>
          
        </div>
        <a class="date meta" href="/blogs/news/8928423-dieter-rams">Sep 03, 2013</a>
      </article>
      
      <article>
        <h1><a class="title" href="/blogs/news/8928299-krule-office-showroom">Krule Office Showroom</a></h1>
        <div class="rte content">
          
            <p><img src="<?php echo _IMAGE_PATH;?>krule-office-showroom-thumb.jpg?393" alt="" /></p>
<p><span>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</span></p>
          
        </div>
        <a class="date meta" href="/blogs/news/8928299-krule-office-showroom">Sep 03, 2013</a>
      </article>
      

    </div>

    

  </div>


