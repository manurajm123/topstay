 <div class="uk-panel uk-panel-box  uk-width-1-2 uk-container-center uk-text-center white_bg">
	 
	<?php echo form_open('register', array( 'name' => 'registerfrm', 'id' => 'registerfrm',
											'method' => 'post', 
											'enctype' => 'application/x-www-form-urlencoded',
											 ) );?>
        <fieldset>
            
            <h2>Create an account</h2>
                              
            <div class="uk-form-icon">
            	<i class="uk-icon-envelope-o"></i>
                <input  class="uk-form-width-large uk-form-large" type="text" 
                placeholder="Email Address" name="email" id="email" value="<?php echo set_value('email'); ?>">
            </div>
            
            <div class="uk-form-icon">
            	<i class="uk-icon-smile-o"></i>
                <input class="uk-form-width-large uk-form-large" type="text" 
                placeholder="First Name" name="fname" id="fname" value="<?php echo set_value('fname'); ?>">
            </div>
            
            <div class="uk-form-icon">
            	<i class="uk-icon-smile-o"></i>
                <input class="uk-form-width-large uk-form-large" type="text" 
                placeholder="Last Name" name="lname" id="lname" value="<?php echo set_value('lname'); ?>">
            </div>
            
             <div class="uk-form-icon">
             	<i class="uk-icon-mobile-phone"></i>
                <input class="uk-form-width-large uk-form-large" type="text" 
                placeholder="Mobile number" name="mobilenumber" id="mobilenumber" value="<?php echo set_value('mobilenumber'); ?>">
            </div>
            
             <div class="uk-form-icon">
             	<i class="uk-icon-phone"></i>
                <input class="uk-form-width-large uk-form-large" type="text" 
                placeholder="Landline number" name="phonenum" id="phonenum" value="<?php echo set_value('phonenum'); ?>">
            </div>
            <div class="uk-form-icon">
            	<i class="uk-icon-unlock-alt"></i>
                <input class="uk-form-width-large uk-form-large" type="password" placeholder="Password" name="pass" id="pass">
            </div>
            
            <div class="uk-form-row">
               <input type="submit" name="register-submit" value="Submit" id="register-submit"/>
            </div>
            <p class="font_12">
            By signing up, I agree to Topstay's Terms of Service, Privacy Policy, 
            Guest Refund Policy, and Host Guarantee Terms.
            </p>
            <hr class="uk-article-divider">
            <a href=""><img src="<?php echo _IMAGE_PATH;?>fb-signup.png"/></a>
        </fieldset>

    <?php echo form_close(); ?>
     
</div>