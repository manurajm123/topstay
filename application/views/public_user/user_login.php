 <div class="uk-panel uk-panel-box  uk-width-1-2 uk-container-center uk-text-center white_bg login-box">
  <?php echo form_open('login', array( 'name' => 'loginfrm', 'id' => 'loginfrm',
											'method' => 'post', 
											'enctype' => 'application/x-www-form-urlencoded',
											 ) );?>
 
            <h2>Registered Customers</h2>
           
            <hr class="uk-article-divider">
            
            <div class="uk-form-row">  
                <div class="uk-form-icon">
                     <i class="uk-icon-envelope-o"></i>
                     <input type="text" placeholder="Email Address" class="uk-form-large" name="email" id="email" value="<?php echo set_value('email'); ?>">
                </div>
            </div>
            
            <div class="uk-form-row">  
                <div class="uk-form-icon">
                     <i class="uk-icon-unlock-alt"></i>
                     <input type="password" placeholder="Password" class="uk-form-large" name="pass" id="pass">
                </div>
            </div>
            <div class="uk-form-row"><input type="checkbox" name="remember-me"/> Remember me</div> 
            
            <div class="uk-form-row submit-ht">
               <input type="submit" name="login-submit" value="Submit" id="login-submit"/>
            </div>
            
            <div class="uk-form-row"><a href="">Forgot Password?</a></div>   
            
             <hr class="uk-article-divider">
             <a href=""><img src="<?php echo _IMAGE_PATH;?>fb-login.png"/></a>
            
            
            <p class="font_14">
           		Don't have an account? <a href="<?php echo site_url('register');?>">Create an account</a>
            </p>
            
    <?php echo form_close(); ?>
</div>
      

