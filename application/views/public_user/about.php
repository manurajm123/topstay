  
 <div class="breadcrumbs meta">

   <span><a href="/">Home</a></span>
   
      <span class="sep">/</span> <span><a href="#" title="">News</a></span>
      <span class="sep">/</span> <span>Danish Modern for Kids</span>
   

</div>
    

    
      <div class="page-header">
  <h1><a href="#">News</a></h1>
</div>

<article class="blog-article">

  
  

  <header>
    <h1 class="article-title">
      
        Danish Modern for Kids
      
    </h1>
  </header>

  <aside class="article-meta meta">

    <span class="date">
      
        Sep 04, 2013
      
    </span>

    

    

    
    <div class="blog-pagination">
        <a class="previous" href="#">Previous Post</a>
        
        
    </div>
    
  </aside>

  <div class="rte article-content">
    
      <p><img src="<?php echo _IMAGE_PATH;?>geo-large.jpg" alt="" /></p>
<p>Nullam quis risus eget urna mollis ornare vel eu leo. Donec ullamcorper nulla non metus auctor fringilla. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
    

    
    <p class="article-author">
      Written by
      
        Liam Sarsfield
      
    </p>
    
  </div>

  
  <footer class="article-footer">
    



<div class="share-buttons " data-permalink="#">

  <a target="_blank" href="#" class="share-facebook"><span>Like</span> <span class="share-count"></span></a>

  <a target="_blank" href="#" class="share-twitter"><span>Tweet</span> <span class="share-count"></span></a>

  

  <a target="_blank" href="#" class="share-google"><span></span> <span class="share-count">+1</span></a>

</div>

  </footer>
  

</article>



  <div id="comments" class="comments-wrap">

    <div class="comments-read">

      <!-- Section title -->
      <h4 class="title">
        

        Comments on this post <span class="count">&nbsp;(
          
            0
          
        )</span>
      </h4>

      

    </div>

    <div class="comments-submit">

      <!-- Section title -->
      <h5 class="title">Leave a comment</h5>

      <form method='post' action='/blogs/news/8944941-danish-modern-for-kids/comments' id='article-8944941-comment-form' class='comment-form' accept-charset='UTF-8'><input name='form_type' type='hidden' value='new_comment'/><input name="utf8" type="hidden" value="✓"> 

        

        <div class="input-wrapper author">
          <label>Name</label>
          <input type="text" placeholder="Name" value="" name="comment[author]" class="field ">
        </div>

        <div class="input-wrapper email">
          <label>Email</label>
          <input type="email" placeholder="your@email.com" value="" name="comment[email]" class="field ">
        </div>

        <div class="input-wrapper body">
          <label>Message</label>
          <textarea name="comment[body]" class="field "></textarea>
        </div>

        <div class="input-wrapper">
          <input type="submit" value="Submit">
        </div>

      </form>
    </div>

  </div>





<div class="post-pagination">
	 <a href="#" class="newer-post button secondary"><span>&#57361;</span> Newer Post</a>
</div>
 