<div class="uk-panel uk-panel-box  uk-width-1-2 uk-container-center uk-text-center white_bg login-box">

<?php  echo form_open('webadmin',  array( 'class' => 'uk-form' ) ) ?>
	 
    <fieldset>
        <legend class="uk-text-success"><i class="uk-icon-lock"></i> TOPSTAY ADMINISTRATION LOGIN</legend>
        <br>
        <div class="uk-form-row">
            <input type="email" placeholder="Admin Email" class="uk-form-large uk-form-width-large">
        </div>
        <div class="uk-form-row">
            <input type="password" placeholder="Admin Password" class="uk-form-large uk-form-width-large">
        </div>
         
        <div class="uk-form-row">
           <input type="submit" name="admin-login" value="Submit" id="admin-login">
        </div>
        
    </fieldset>
 
<?php  echo form_close(); ?>

</div>
