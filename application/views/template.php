<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7 lt-ie10" lang="en"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie10" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 lt-ie10" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie9 lt-ie10 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js"> <!--<![endif]-->
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>TopStay | Welcome</title>
  <meta name="viewport" content="width=device-width" />
 
  <!-- Theme CSS
  ///////////////////////////////////////// -->
  <link href="<?php echo _CSS_PATH;?>theme.css?777" rel="stylesheet" type="text/css"  media="all"  />
  <link href="<?php echo _CSS_PATH;?>uikit.almost-flat.css?777" rel="stylesheet" type="text/css"  media="all"  />
  <link href="<?php echo _CSS_PATH;?>override.css?777" rel="stylesheet" type="text/css"  media="all"  />
  
  <!-- Javascript -->
  <script src="<?php echo _SCRIPT_PATH;?>modernizr-2.6.2.min.js?777" type="text/javascript"></script>
  <script src="<?php echo _SCRIPT_PATH;?>jquery.js?777" type="text/javascript"></script>
  <script src="<?php echo _SCRIPT_PATH;?>uikit.js?777" type="text/javascript"></script>
 
  <?php echo $_scripts ?>
  <?php echo $_styles ?>
 
  <script>
    var base_url  = '<?php echo site_url()?>';
  </script>
  <!-- Store object
  ///////////////////////////////////////// -->
  <script type="text/javascript">
    settings = {}

    // Home Page
    settings.showSlideshow       = true;
    settings.slideshowNavigation = true;
    settings.slideshowAutoPlay   = false;
    settings.slideDuration       = '4';
    settings.featuredCollections = true;
    settings.collectionAlignment = 'centered';
  </script>

</head>

<body class="template-index" >
  
<div class="header-wrapper">
  <header class="main-header clearfix accounts-disabled">

    <!-- Store logo / title
    =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= -->
    <div class="branding">
      
        <h1 class="logo-image"><a href="<?php echo site_url('welcome');?>">
          <img class="regular-logo" alt="Kapsel Contemporary's logo" src="<?php echo _IMAGE_PATH;?>logo.png?777">
 		  <img class="retina-logo" alt="Kapsel Contemporary's retina logo" src="<?php echo _IMAGE_PATH;?>logo-retina.png?777">
          
        </a></h1>
      
    </div>

    <div class="mobile-nav">
      <span class="mobile-nav-item mobile-nav-navigate">&#57349;</span>
      <a href="/cart" class="mobile-nav-item mobile-nav-cart">&#57347;</a>
      <a href="/account" class="mobile-nav-item mobile-nav-account">&#57346;</a>
      <span class="mobile-nav-item mobile-nav-search search-toggle">&#57345;</span>
      <form class="search-form mobile-search-form" action="/search" method="post">
        <input class="search-input" name="q" type="text" placeholder="Search" value="">
      </form>
    </div>

    <!-- Main navigation
    =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= -->
    <nav class="navigation">
      <ul>
        <li class="first">
        
        	<form class="uk-form" id="where-you-going">
                <div class="uk-form-icon">
                    <i class="uk-icon-search"></i>
                    <input type="text" name="where-go" placeholder="Where are you going?" autocomplete="off" class="uk-form-width-medium">
                </div>
            </form>
            
            </li>
		<li class="has-dropdown" style="margin-left:15px">
			<a href="<?php echo site_url('welcome/all');?>">Browse</a>
					<ul class="dropdown-list">
						<li class="list-item has-dropdown">
							<a href="<?php echo site_url('welcome/popular');?>">Popular</a>
						</li>
					    <li class="list-item ">
							<a href="<?php echo site_url('welcome/friends');?>">Friends</a>
						</li>
						<li class="list-item ">
							<a href="<?php echo site_url('welcome/neighborhood');?>">Neighborhood</a>
						</li>  
					</ul>
        </li>
		<li class="last" ><a href="<?php echo site_url('welcome/about');?>">About</a></li>
		 
      </ul>
    </nav>

	<?php if($this->session->userdata('_USER_LOGGED_IN') == true) { ?>
    
    <div style="float:right !important;position: relative;top: 3px;">
     <nav class="navigation"> <ul>
        <li class="has-dropdown" style="margin-left:15px">
			<a href="#"><i class="uk-icon-user"></i> <?php echo $this->session->userdata('_USER_NAME')?></a>
					<ul class="dropdown-list">
						<li class="list-item has-dropdown">
							<a href="#">Dashboard</a>
						</li>
					    <li class="list-item ">
							<a href="#">My Listings</a>
						</li>
                        <li class="list-item ">
							<a href="#">Account</a>
						</li>
						<li class="list-item ">
							<a href="<?php echo site_url('login/logout');?>"><i class="uk-icon-lock"></i> Logout</a>
						</li>  
					</ul>
        </li>
        </ul>
      </nav>
    </div>
    
    <?php } ?>
    
    <!-- Action links
    =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= -->
    <div class="action-links">
    	   
      <?php if($this->session->userdata('_USER_LOGGED_IN') == false) { ?>
        
        <a href="<?php echo site_url('register');?>"><span>SIGNUP</span></a>
      	<a href="<?php echo site_url('login');?>"><span><i class="uk-icon-unlock-alt uk-icon-small"></i> LOGIN</span></a>
        
      <?php } ?>
      
      <a class="list-space" href="<?php echo site_url('space');?>"><span><i class="uk-icon-map-marker uk-icon-small"></i> LIST YOUR SPACE</span></a>
      
    </div>

  </header>

  
</div>

  
    

	<!-- SLIDESHOW -->
		<?php echo (isset($slide_show) && !empty($slide_show)) ? $slide_show : '<div class="seperator"></div>';?>
    <!-- close SLIDESHOW -->
   
    
  <div class="main-content container">
  	
    <!-- Error Template -->
    	<?php if(isset($error_template) && !empty($error_template) ) { ?>
        		
                <div class="uk-alert uk-alert-danger" data-uk-alert="">
                    <a href="" class="uk-alert-close uk-close"></a>
                    <p><?php echo @$error_template;?></p>
                </div>
            
        <?php } ?>
    <!-- close Error Template -->
    
    <?php echo @$content;?>
  </div>

  





<div class="footer-wrapper  module-count-3">

  <footer class="main-footer">

    
    <div class="contact footer-module">
      <h4>TopStay</h4>
 	  <p>Kaivokatu 2<br>Helsinki <br>FI-00100 Finland</p>
      <p><a href="mailto:info@topstay.com">info@topstay.com</a></p>  
    </div>
    
    <div class="links footer-module">
          <ul>
            <li><a href="/" title="">Home</a></li>
            <li><a href="/collections/all" title="">Catalog</a></li>
            <li><a href="/blogs/news" title="">News</a></li>
            <li><a href="/pages/documentation" title="">Theme features</a></li>
         </ul>
        
         <ul>
          <li><a href="/pages/about-us" title="">About</a></li>
          <li><a href="/pages/custom-page" title="">FAQs</a></li>
          <li><a href="/pages/custom-page" title="">Returns</a></li>
         </ul>
    </div>
    

    
    <div class="connect footer-module">
 
      <h4>Connect with us</h4>
       
      <ul class="social-options">
        
          <li class="twitter"><a href="http://twitter.com/topstay" target="_blank">&#57600;</a></li>
        
        
          <li class="facebook"><a href="http://facebook.com/topstay" target="_blank">&#57601;</a></li>
        
        
          <li class="interest"><a href="http://pinterest.com/topstay" target="_blank">&#57602;</a></li>
        
        
          <li class="instagram"><a href="http://instagram.com/topstay" target="_blank">&#57603;</a></li>
        
        
          <li class="fancy"><a href="http://fancy.com/topstay" target="_blank">&#57604;</a></li>
        
        
          <li class="rss"><a href="/blogs/topstay.atom" target="_blank">&#57605;</a></li>
        
      </ul>

      

      
        <form action="" method="post" name="mc-embedded-subscribe-form" class="validate mailing-list-signup" target="_blank">

          <input class="submit" name="subscribe" type="submit" value="Subscribe">

          <div class="email-container">
            
            <input class="email" name="EMAIL" type="email" placeholder="Email address" value="">
            

            
          </div>

        </form>
      

    </div>
    

  </footer>
</div>

<div class="copyright-wrap">
  <div class="copyright">
    <p>Copyright &copy; 2014 TopStay. <a href="#" target="_blank">E-commerce by TopStay</a>.</p>
    <ul class="payment-options">
      
      
      
      
        <li class="visa">&#57856;</li>
      
      
        <li class="mastercard">&#57857;</li>
      
      
      
        <li class="dk">&#57865;</li>
      
      
      
        <li class="google-wallet">&#57864;</li>
      
      
        <li class="paypal">&#57859;</li>
      
      
      
      
        <li class="stripe">&#57861;</li>
      
    </ul>
  </div>
</div>

  <!-- Theme JS
  ///////////////////////////////////////// -->
  <script src="<?php echo _SCRIPT_PATH;?>option_selection.js?5776ca9f884582431f1ce6311f0b8649fdcb425e" type="text/javascript"></script>
  <script src="<?php echo _SCRIPT_PATH;?>plugins.js?777" type="text/javascript"></script>
  <script src="<?php echo _SCRIPT_PATH;?>site.js?777" type="text/javascript"></script>

</body>
</html>



	
	
	
