// JavaScript Document
   
  window.onload = function(){
  	 $('#searchfrm').submit(function(evt){
  	 	evt.preventDefault();
  	 });
  	 initialize();	
  }
 
  function initialize() {
  	var query			 = false;
  	$('.searchbox').each(function(key,elem){
  		var input		 = document.getElementById( 'query' + key );
  		var autocomplete = new google.maps.places.Autocomplete(
	        /** @type {HTMLInputElement} */( input ),
	        { types: ['geocode'] });
	    google.maps.event.addListener(autocomplete, 'place_changed', function() {
	    	var place 				= 	autocomplete.getPlace();
	    	var co_ordinates		=	place.geometry.location;	 
	    	if(input.value) query 	= 	true; 
 			if(query == true){
 				var url  = base_url+"/search?query=" + input.value + '&lc_lat=' + co_ordinates.d + '&lc_lng=' + co_ordinates.e ;
			    document.getElementById('searchfrm').action = url;
			    document.getElementById('searchfrm').submit();
		  	}
	    });
  	});
  }

