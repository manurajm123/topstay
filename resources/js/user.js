// JavaScript Document
 
$(function(){
    
	$( "#registerfrm,#loginfrm" ).on( "submit", function() { 

	   var has_empty = false;
	
	   $(this).find( 'input[type!="submit"]' ).each(function () {
	
		  if ( ! $(this).val() ) { 
		  	has_empty = true; $(this).addClass('uk-form-danger'); $(this).focus(); return false; 
		  }
		  else if( $(this).attr( 'id' ) == 'email' ){
			  
			  $(this).removeClass('uk-form-success');
			  
			  if( !IsEmail($(this).val()) ){
				has_empty = true; $(this).addClass('uk-form-danger'); $(this).focus(); return false; 	  
			  } 
			  else $(this).addClass('uk-form-success');
			  
		  }
		  
		  else if( $(this).attr( 'id' ) == 'mobilenumber' ){
			  
			  $(this).removeClass('uk-form-success');
			  
			  var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
			  
			  if( $(this).val().length != 10 ) {
			  	has_empty = true; $(this).addClass('uk-form-danger'); $(this).focus(); return false; 	  
			  }
			  else if(!numberRegex.test($(this).val())) {
				has_empty = true; $(this).addClass('uk-form-danger'); $(this).focus(); return false; 	  
			  }
			  else $(this).addClass('uk-form-success');
			   
		  }
		  
		  else if( $(this).attr( 'id' ) == 'pass' ){
			   
			   $(this).removeClass('uk-form-success');
			   
			   if( $(this).val().length < 5 ) {
			  	has_empty = true; $(this).addClass('uk-form-danger'); $(this).focus(); return false; 	  
			   }
			   else $(this).addClass('uk-form-success');
			    
		  }
		   
		  else $(this).addClass('uk-form-success');
		  
	   });
		 
	   if ( has_empty ) { return false; }
	});
	 
	
	 

});

function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

 